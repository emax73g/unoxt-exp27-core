truncate -s 802560 ./unoxt_top.bin
cp ./unoxt_top.bin ./spectrum.zxt

truncate -s 802560 ./unoxt2_top.bin
cp ./unoxt2_top.bin ./spectrum.xt2

truncate -s 802816 ./unoxt_special_top.bin
cp ./unoxt_special_top.bin ./special.zxt

truncate -s 802816 ./unoxt2_special_top.bin
cp ./unoxt2_special_top.bin ./special.xt2
