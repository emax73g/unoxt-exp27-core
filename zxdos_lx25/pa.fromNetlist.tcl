
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name zxdos_lx25 -dir "/home/ise/share/unoxt.exp27/zxdos_lx25/planAhead_run_3" -part xc6slx25ftg256-2
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "/home/ise/share/unoxt.exp27/zxdos_lx25/unoxt_top.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/ise/share/unoxt.exp27/zxdos_lx25} }
set_property target_constrs_file "unoxt_pins.ucf" [current_fileset -constrset]
add_files [list {unoxt_pins.ucf}] -fileset [get_property constrset [current_run]]
link_design
