`timescale 1ns / 1ns
`default_nettype none

//    This file is part of the ZXUNO Spectrum core. 
//    Creation date is 02:28:18 2014-02-06 by Miguel Angel Rodriguez Jodar
//    (c)2014-2020 ZXUNO association.
//    ZXUNO official repository: http://svn.zxuno.com/svn/zxuno
//    Username: guest   Password: zxuno
//    Github repository for this core: https://github.com/mcleod-ideafix/zxuno_spectrum_core
//
//    ZXUNO Spectrum core is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    ZXUNO Spectrum core is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//e
//    You should have received a copy of the GNU General Public License
//    along with the ZXUNO Spectrum core.  If not, see <https://www.gnu.org/licenses/>.
//
//    Any distributed copy of this file must keep this notice intact.

//Max
//UnoXT Generation Selector
//see in unoxt_gen.v
`include "unoxt_gen.v"

`ifdef unoxt
	module unoxt_top (
`elsif unoxt2
	module unoxt2_top (
`elsif unoxt_special
	module unoxt_special_top (
`elsif unoxt2_special
	module unoxt2_special_top (
`else
	module unoxt_top (
`endif

   input wire clock_50_i,

   output wire [4:0] rgb_r_o,
   output wire [4:0] rgb_g_o,
   output wire [4:0] rgb_b_o,
   output wire hsync_o,
   output wire vsync_o,
   input wire ear_port_i,
   output wire mic_port_o,
   inout wire ps2_clk_io,
   inout wire ps2_data_io,
   inout wire ps2_pin6_io,
   inout wire ps2_pin2_io,
   output wire audioext_l_o,
   output wire audioext_r_o,

   inout wire esp_gpio0_io,
   inout wire esp_gpio2_io,
	output wire esp_tx_o,
   input wire esp_rx_i,
 
   output wire [20:0] ram_addr_o,
	output wire ram_lb_n_o,
	output wire ram_ub_n_o,
   inout wire [15:0] ram_data_io,
   output wire ram_oe_n_o,
   output wire ram_we_n_o,
   output wire ram_ce_n_o,
   
   output wire flash_cs_n_o,
   output wire flash_sclk_o,
   output wire flash_mosi_o,
   input wire flash_miso_i,
	output wire flash_wp_o,
	output wire flash_hold_o,
   
   input wire joyp1_i,
   input wire joyp2_i,
   input wire joyp3_i,
   input wire joyp4_i,
   input wire joyp6_i,
   output wire joyp7_o,
   input wire joyp9_i,
	
   input wire btn_divmmc_n_i,
   input wire btn_multiface_n_i,

   output wire sd_cs0_n_o,    
   output wire sd_sclk_o,     
   output wire sd_mosi_o,    
   input wire sd_miso_i,

	output wire led_red_o,
	output wire led_yellow_o,
   output wire led_green_o,   // nos servir como testigo de uso de la SPI
	output wire led_blue_o,
	
	//Max
	output wire [8:0] test 

   );
   
   //Max
	`define DEFAULT_100_LEDS;
	
	`ifdef ROUND_DIF_LEDS
		//3mm round diffused LEDs
		localparam ledRedK = 16'd20;
		localparam ledYellowK = 16'd50;
		localparam ledGreenK = 16'd12;
		localparam ledBlueK = 16'd50;
	`elsif SQUARE_LEDS
		//square color LEDs
		localparam ledRedK = 16'd20;
		localparam ledYellowK = 16'd33;
		localparam ledGreenK = 16'd100;
		localparam ledBlueK = 16'd20;
	`else
		//default 100% LEDs
		localparam ledRedK = 16'd100;
		localparam ledYellowK = 16'd100;
		localparam ledGreenK = 16'd100;
		localparam ledBlueK = 16'd100;
	`endif
	wire [3:0] cpu_speed;
 
   wire sysclk;
   wire [2:0] pll_frequency_option;
   
   clock_generator relojes_maestros
   (// Clock in ports
    .CLK_IN1            (clock_50_i),
    .pll_option         (pll_frequency_option),
    // Clock out ports
    .sysclk             (sysclk)
    );

   wire [2:0] ri, gi, bi;
   wire [5:0] ro, go, bo;
   wire hsync_pal, vsync_pal, csync_pal;
   wire vga_enable, scanlines_enable;
   wire clk14en_tovga;
	
	assign joyp7_o = 1'b1;
	assign ram_oe_n_o = 1'b0;
	assign ram_ce_n_o = 1'b0;
	assign ram_lb_n_o = 1'b0;
	assign ram_ub_n_o = 1'b1;
	assign ram_data_io[15:8] = 8'bz;
	
	assign flash_wp_o = 1'b1;
	assign flash_hold_o = 1'b1;
	
   zxuno #(.FPGA_MODEL(3'b011), .MASTERCLK(28000000)) la_maquina (
    .sysclk(sysclk),
    .power_on_reset_n(1'b1),  // slo para simulacin. Para implementacion, dejar a 1
    .r(ri),
    .g(gi),
    .b(bi),
    .hsync(hsync_pal),
    .vsync(vsync_pal),
    .csync(csync_pal),
    .clkps2(ps2_clk_io),
    .dataps2(ps2_data_io),
	 .ear_ext(~ear_port_i),  // negada porque el hardware tiene un transistor inversor
    .audio_out_left(audioext_l_o),
    .audio_out_right(audioext_r_o),
    
    .midi_out(),
    .clkbd(),
    .wsbd(),
    .dabd(),
    
    .uart_tx(esp_tx_o),
    .uart_rx(esp_rx_i),
    .uart_rts(esp_gpio2_io),

    .sram_addr(ram_addr_o),
    .sram_data(ram_data_io[7:0]),
    .sram_we_n(ram_we_n_o),
    
    .flash_cs_n(flash_cs_n_o),
    .flash_clk(flash_sclk_o),
    .flash_di(flash_mosi_o), 
    .flash_do(flash_miso_i),
    
    .sd_cs_n(sd_cs0_n_o),
    .sd_clk(sd_sclk_o),
    .sd_mosi(sd_mosi_o),
    .sd_miso(sd_miso_i),
    
    .joy1up(joyp1_i),
    .joy1down(joyp2_i),
    .joy1left(joyp3_i),
    .joy1right(joyp4_i),
    .joy1fire1(joyp6_i),
    .joy1fire2(joyp9_i),    
	 
    .joy2up(1'b1),
    .joy2down(1'b1),
    .joy2left(1'b1),
    .joy2right(1'b1),
    .joy2fire1(1'b1),
    .joy2fire2(1'b1),    

    .mouseclk(ps2_pin6_io),
    .mousedata(ps2_pin2_io),
    
    .clk14en_tovga(clk14en_tovga),
    .vga_enable(vga_enable),
    .scanlines_enable(scanlines_enable),
    .freq_option(pll_frequency_option),
	 //Max
	 .cpu_speed(cpu_speed),
    
    .ad724_xtal(),
    .ad724_mode()	 
    );

	vga_scandoubler #(.CLKVIDEO(14000)) salida_vga (
		.clk(sysclk),
    .clk14en(clk14en_tovga),
    .enable_scandoubling(vga_enable),
    .disable_scaneffect(~scanlines_enable),
		.ri(ri),
		.gi(gi),
		.bi(bi),
		.hsync_ext_n(hsync_pal),
		.vsync_ext_n(vsync_pal),
    .csync_ext_n(csync_pal),
		.ro(ro),
		.go(go),
		.bo(bo),
		.hsync(hsync_o),
		.vsync(vsync_o)
   );	 
   
   //Max
	wire[15:0] turboLed;
	wire flashLed, wiFiLed;
	assign turboLed = (cpu_speed == 4'b0000) ? 16'd0 : ((cpu_speed == 4'b0001) ? 16'd33 : (cpu_speed == 4'b0010) ? 16'd66 : (cpu_speed == 4'b0011) ? 16'd100 : (cpu_speed[2] == 1'b1) ? 16'd100 : 16'd0);
   assign flashLed = (!flash_cs_n_o || !sd_cs0_n_o);
	assign wiFiLed = !esp_rx_i;
 
	ledPWM ledRed(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(1'b1),
		.y1(16'd100),
		.y2(ledRedK),	
		.led(led_red_o)
    );

	ledPWM ledYellow(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(1'b1),
		.y1(turboLed),
		.y2(ledYellowK),	
		.led(led_yellow_o)
    );

	ledPWM ledGreen(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(flashLed),
		.y1(16'd100),
		.y2(ledGreenK),	
		.led(led_green_o)
    );

	ledPWM ledBlue(
		.nReset(1'b1),
		.clock(sysclk),
		.enable(wiFiLed),
		.y1(16'd100),
		.y2(ledBlueK),	
		.led(led_blue_o)
    );

   
   assign rgb_r_o = ro[5:1];
   assign rgb_g_o = go[5:1];
   assign rgb_b_o = bo[5:1];
	
	assign mic_port_o = 1'b1;
	
	//Max
	assign test[0] = 1'b1;
   assign test[1] = 1'b1;
   assign test[2] = 1'b1;
   assign test[3] = 1'b1;
   assign test[4] = 1'b1;
   assign test[5] = 1'b1;
   assign test[6] = 1'b1;
   assign test[7] = 1'b1;
   assign test[8] = 1'b1;
	
endmodule
